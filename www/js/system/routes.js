// Shoney's Test App

// Declare routes here

"use strict";

angular.module('shoneys')
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html'
    })
    .state('app.home', {
      url: '/home',
      views : {
        'contents': {
          templateUrl: 'templates/home.html',
          controller: ''
        }
      }
    })
    .state('app.offers', {
      url: '/offers',
      views : {
        'contents': {
          templateUrl: 'templates/offers.html',
          controller: ''
        }
      }
    })
    .state('app.feedback', {
      url: '/feedback',
      views : {
        'contents': {
          templateUrl: 'templates/feedback.html',
          controller: ''
        }
      }
    })
    .state('app.feedback2', {
      url: '/feedback2',
      views : {
        'contents': {
          templateUrl: 'templates/feedback2.html',
          controller: ''
        }
      }
    })
    .state('app.rewards', {
      url: '/rewards',
      views : {
        'contents': {
          templateUrl: '',
          controller: ''
        }
      }
    })
    .state('app.location', {
      url: '/location',
      views : {
        'contents': {
          templateUrl: '',
          controller: ''
        }
      }
    })
    .state('app.menu', {
      url: '/menu',
      views : {
        'contents': {
          templateUrl: '',
          controller: ''
        }
      }
    })
    .state('app.aboutme', {
      url: '/aboutme',
      views : {
        'contents': {
          templateUrl: '',
          controller: ''
        }
      }
    })

  $urlRouterProvider.otherwise('/login');
})